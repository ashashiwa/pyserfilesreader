<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Biblitoh&#xe8;que de lecture&#xa;de fichier ser&#xa;python" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1612459752769"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<node TEXT="cr&#xe9;ation d&apos;un objet serf_file(fichier, mode)" POSITION="right" ID="ID_1554836932" CREATED="1612459897112" MODIFIED="1612460220261">
<edge COLOR="#ff0000"/>
<font SIZE="14" BOLD="false"/>
<node TEXT="m&#xe9;thode read() -&gt; renvoiem l&apos;image sous forme de numpy array et avance le curseur de 1" ID="ID_1371090644" CREATED="1612459946718" MODIFIED="1612460403984"/>
<node TEXT="attribut mode : par d&#xe9;faut : &apos;monochrome&apos; //pour le moment" ID="ID_142163594" CREATED="1612460221288" MODIFIED="1612460245569"/>
<node TEXT="m&#xe9;thode read_header() -&gt; renvoie le dictionnaire des valeurs du header" ID="ID_1417405626" CREATED="1612460137378" MODIFIED="1612460163318"/>
<node TEXT="m&#xe9;thode read_frame_at_pos(nb) -&gt; renvoie un array numpy contenant la frame &#xe0; la position nb" ID="ID_1612228990" CREATED="1612460163945" MODIFIED="1612460213445"/>
<node TEXT="m&#xe9;thode length() -&gt;renvoie le nombre de frames" ID="ID_600823802" CREATED="1612460259895" MODIFIED="1612460333179"/>
<node TEXT="m&#xe9;thode getWidth() -&gt; renvoie la largeur d&apos;une frame" ID="ID_1406864542" CREATED="1612460312894" MODIFIED="1612460412866"/>
<node TEXT="m&#xe9;thode getHeight() -&gt; renvoie la hauteur d&apos;une frame" ID="ID_1574322189" CREATED="1612460345277" MODIFIED="1612460415241"/>
<node TEXT="m&#xe9;thode getCursor() -&gt; renvoie la position actuelle du curseur" ID="ID_1988291460" CREATED="1612460421667" MODIFIED="1612460446911"/>
<node TEXT="m&#xe9;thode setCursor(nb:int) -&gt; mets le curseur &#xe0; la position nb. return 0 si OK, -1 sinon (si erreur ou si nb&gt; length)" ID="ID_362461536" CREATED="1612460452314" MODIFIED="1612460504118"/>
<node TEXT="methode save_png(fichier) -&gt; sauvegarde la frame au curseur actuel en pbg" ID="ID_1186534286" CREATED="1612461061133" MODIFIED="1612462975525"/>
<node TEXT="m&#xe9;thode forward() -&gt; augment le curseur d&apos;une image" ID="ID_815314517" CREATED="1612462977232" MODIFIED="1612463064715"/>
<node TEXT="m&#xe9;thode reverse() -&gt; diminue le curseur d&apos;une image" ID="ID_417288251" CREATED="1612463058279" MODIFIED="1612463096216"/>
<node TEXT="attributs importants" ID="ID_974337226" CREATED="1612463104070" MODIFIED="1612463159737">
<node TEXT="self._cursor" ID="ID_456054482" CREATED="1612463161436" MODIFIED="1612463309949">
<node TEXT="position de l&apos;image qui va &#xea;tre lue" ID="ID_1823006335" CREATED="1612463178292" MODIFIED="1612463198016"/>
</node>
<node TEXT="self._offset (priv&#xe9;)" ID="ID_173507767" CREATED="1612463169460" MODIFIED="1612463304696">
<node TEXT="position du curseur mais en octets" ID="ID_757302831" CREATED="1612463226203" MODIFIED="1612463238719"/>
</node>
</node>
</node>
<node TEXT="outils Numpy" POSITION="left" ID="ID_1745660025" CREATED="1612460525049" MODIFIED="1612460939634">
<edge COLOR="#0000ff"/>
<node TEXT="   EXEMPLE :  entete = {}&#xa;    b=np.fromfile(serfile, dtype=&apos;int8&apos;,count=14)&#xa;    if debug :&#xa;        print(b)&#xa;    FileID=b.tobytes().decode()&#xa;    if debug :&#xa;        print (&apos;file ID: &apos;,FileID)&#xa;    entente[&apos;file ID&apos;] = FileID&#xa;   &#xa;    offset=14&#xa;    LuID=np.fromfile(serfile, dtype=np.uint32, count=1, offset=offset)&#xa;    if debug :&#xa;        print (LuID[0])&#xa;    entete[&apos;LuID&apos;] = LuID[0]&#xa;   &#xa;    offset=offset+4&#xa;    ColorID=np.fromfile(serfile, dtype=&apos;uint32&apos;, count=1, offset=offset)&#xa;    if debug :&#xa;        print(ColorID[0])&#xa;    entete[&apos;ColorID&apos;] = ColorID[0]" ID="ID_1592490279" CREATED="1612460587679" MODIFIED="1612460939632" HGAP="120" VSHIFT="-210"/>
</node>
<node TEXT="sp&#xe9;cifications fichier SER" POSITION="left" ID="ID_1876602670" CREATED="1612460831218" MODIFIED="1612460944753">
<edge COLOR="#00ff00"/>
<node TEXT="http://www.grischa-hahn.homepage.t-online.de/astro/ser/SER%20Doc%20V3b.pdf" ID="ID_1957603379" CREATED="1612460909912" MODIFIED="1612460944753" HGAP="30" VSHIFT="-10"/>
</node>
</node>
</map>
